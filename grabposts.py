from selenium import webdriver
from bs4 import BeautifulSoup
import requests
from wordpress_xmlrpc import Client
from wordpress_xmlrpc import WordPressPost
from wordpress_xmlrpc.methods import posts
import os
import creds
from dateutil.parser import parse
import re


def selenium_grab_posts():
    # define webdriver and blog url
    if os.name == 'nt':
        driver_path = '.\\driver\\chromedriver.exe'
    else:
        driver_path = './driver/chromedriver'
    driver = webdriver.Chrome(executable_path=driver_path)
    driver.get('https://3cx.com/blog')

    posts_container = driver.find_element_by_id('content')
    article = posts_container.find_elements_by_tag_name('article')

    list_of_posts = []
    for post in article:
        title = post.find_element_by_tag_name('h2')
        link = post.find_element_by_tag_name('a').get_attribute('href')
        date = post.find_elements_by_tag_name('span')

        post_dict = {
            "title": title.text,
            "link": link,
            "date": date[2].text
        }
        list_of_posts.append(post_dict)

    return list_of_posts


def bs4_grab_posts():
    url = 'https://www.3cx.com/blog/'
    r = requests.get(url, headers={'User-Agent': 'Mozilla/5.0'})
    soup = BeautifulSoup(r.content, 'html.parser')

    posts_container = soup.find('section', attrs={'id': 'content'})
    article = posts_container.findAll('article')

    list_of_posts = []

    for post in article:
        title = post.find('h2').text
        link = post.div.div.a.get('href')
        date = post.find('span', attrs={'class': 'fusion-inline-sep'})
        date = date.next_sibling.text
        date = parse(date)
        post_dict = {
            "title": title,
            "link": link,
            "date": date
        }
        list_of_posts.append(post_dict)

    parsed_posts = []

    for post in list_of_posts:
        url = post['link']
        r = requests.get(url, headers={'User-Agent': 'Mozilla/5.0'})
        soup = BeautifulSoup(r.content, 'html5lib')
        post_content = soup.find('div', attrs={'class': 'post-content'})
        post_content_stripped = re.sub('srcset=\".*?\" ', '', str(post_content))
        parsed_post = {
            'title': post['title'],
            'link': post['link'],
            'date': post['date'],
            'content': post_content_stripped
        }
        parsed_posts.append(parsed_post)
    
    return parsed_posts


def connect_to_wp():
    url = creds.creds['url']
    user = creds.creds['user']
    pwd = creds.creds['pwd']
    client = Client(url, user, pwd)

    return client


def get_wp_posts():
    client = connect_to_wp()
    wp_posts_obj = client.call(posts.GetPosts({'number': '200'}))
    wp_posts_list = []

    for post in wp_posts_obj:
        wp_posts_list.append(post.title)
    
    return wp_posts_list


def send_to_wp():
    # connect to wp
    client = connect_to_wp()
    parsed_posts = bs4_grab_posts()
    wp_posts = get_wp_posts()

    for post in parsed_posts[::-1]:
        if post['title'] in wp_posts:
            print(f'{post["title"]} is in the list')
        else:
            print(f'{post["title"]} missing! Adding post!')

            wppost = WordPressPost()
            wppost.title = post['title']
            wppost.content = str(post['content'])
            wppost.id = client.call(posts.NewPost(wppost))

            wppost.post_status = 'draft'
            client.call(posts.EditPost(wppost.id, wppost))


if __name__ == '__main__':
    send_to_wp()
