# 3cx-blog-scraper
A tool to scrape blog posts from 3CX

## Grab latest 3CX blog post
Uses selenium and chrome webdriver to automatically navigate to and grab the titel and content of the post

## Clone post to CloudCo Partner WordPress blog
Takes the source of the 3CX blog post and creates a new WordPress post on CloudCo Partner blog
